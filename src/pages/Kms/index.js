import React, { useState } from "react";
import { validaCPF } from '../../utils/utils.js'
import axios from 'axios';

export default function Kms() {

  // CRIA TODAS AS VARIAVEIS QUE SERÃO ENVIADAS NA REQUISIÇÃO
  const [tipo_credencial, setTipoCredencial] = useState("BRYKMS");
  const [tipo_credencial_sistema, setTipoCredencialSistema] = useState("");
  const [valor_credencial, setValorCredencial] = useState("");
  const [uuid_cert, setUuidCert] = useState("");
  const [uuid_pkey, setUUidPkey] = useState("");
  const [user, setUser] = useState("");
  const [cpf, setCpf] = useState("");
  const [perfil, setPerfil] = useState("BASICA");
  const [algoritmoHash, setAlgoritmoHash] = useState("SHA256");
  const [documento, setDocumento] = useState("");
  const [loading, setLoading] = useState(false);
  const [assinaturaVisivel, setAssinaturaVisivel] = useState("false");
  const [incluirIMG, setIncluirIMG] = useState("false");
  const [incluirTXT, setIncluirTXT] = useState("false");
  const [imagem, setImagem] = useState("");
  const [altura, setAltura] = useState("");
  const [largura, setLargura] = useState("");
  const [coordenadaX, setCoordenadaX] = useState("");
  const [coordenadaY, setCoordenadaY] = useState("");
  const [posicao, setPosicao] = useState("INFERIOR_ESQUERDO");
  const [pagina, setPagina] = useState("PRIMEIRA");
  const [texto, setTexto] = useState("");
  const [incluirCN, setIncluirCN] = useState(false);
  const [incluirCPF, setIncluirCPF] = useState(false);
  const [incluirEmail, setIncluirEmail] = useState(false);
  const [cpfInvalido, setCpfInvalido] = useState(false);


  // FUNCÃO QUE SERÁ EXECUTADA QUANDO FOR CLICADO NO BOTÃO "Assinar"
  async function handleSubmit(event) {

    // PREVINE AÇÕES PADÕRES DE UMA PAGINA REACT
    event.preventDefault();
    // UTILIZA A FUNCÃO "validaCPF" DE UTILS PARA VALIDAR O CPF DO INPUT 
    if (cpf === "" || validaCPF(cpf)) {
      setCpfInvalido(false);
    } else {
      setCpfInvalido(true);
      return;
    }
    // ALTERA A VARIÁVEL LOADING PRA TRUE PARA QUE APAREÇA MENSAGEM DE "Realizando assinatura do documento"
    setLoading(true);
    // CRIA O FORMDATA QUE SERÁ ENVIADO NA REQUISIÇÃO
    const data = new FormData();
    // INCLUIR AS VARIÁVEIS NO FORMDATA AS
    // CREDENCIAIS KMS
    if (!(tipo_credencial === "")) {
      data.append("kms_type", tipo_credencial);
    }
    if (!(valor_credencial === "")) {
      data.append("valor_credencial", valor_credencial);
    }
    if (!(tipo_credencial_sistema === "")) {
      data.append("kms_type_credential", tipo_credencial_sistema);
    }
    if (!(uuid_cert === "")) {
      data.append("uuid_cert", uuid_cert);
    }
    if (!(uuid_pkey === "")) {
      data.append("uuid_pkey", uuid_pkey);
    }
    if (!(user === "")) {
      data.append("user", user);
    }
    // PERFIL DE ASSINATURA (BASICA OU CARIMBO)
    data.append("perfil", perfil);
    // ALGORITMO HASH QUE SERÁ USADO NA CODIFICAÇÃO DO DOCUMENTO
    data.append("algoritmoHash", algoritmoHash);
    // DOCUMENTO NO FORMATO PDF QUE SERÁ ASSINADO
    data.append("documento", documento);
    // SE A ASSINATURA SERÁ VISIVEL NO DOCUMENTO
    data.append("assinaturaVisivel", assinaturaVisivel)

    if (assinaturaVisivel === "true") {
      // ALTURA DO CAMPO DE ASSINATURA
      data.append("altura", altura);
      // LARGURA DO CAMPO DE ASSINATURA
      data.append("largura", largura);
      // COORDENADA NO EIXO X ONDE O CAMPO DE ASSINATURA SERÁ POSICIONADO
      data.append("coordenadaX", coordenadaX);
      // COORDENADA NO EIXO Y ONDE O CAMPO DE ASSINATURA SERÁ POSICIONADO
      data.append("coordenadaY", coordenadaY);
      // POSIÇÃO DO CAMPO DE ASSINATURA (INFERIOR_ESQUERDO, INFERIOR_DIREITO, SUPERIOR_DIREITO, SUPERIOR_ESQUERDO)
      data.append("posicao", posicao);
      // PAGINA DA ASSINATURA
      data.append("pagina", pagina);
      // SE SERÁ INCLUSO OU NÃO O TEXTO
      data.append("incluirTXT", incluirTXT);

      if (incluirTXT === "true") {
        // TEXTO DE ASSINATURA
        data.append("texto", texto);
      }
      // SE SERÁ INCLUSO OU NÃO A IMAGEM
      data.append("incluirIMG", incluirIMG);

      if (incluirIMG === "true") {
        // IMAGEM QUE SERÁ INSERIDA NA ASSINATURA. ACEITA PNG, JPEG E BMP.
        data.append("imagem", imagem);
      }
      // SE SERÁ INCLUSO CPF NA ASSINATURA
      data.append("incluirCPF", incluirCPF);
      // SE SERÁ INCLUSO O NOME DO ASSINANTE NA ASSINATURA (COMMON NAME)
      data.append("incluirCN", incluirCN);
      // SE SERÁ INCLUSO O EMAIL DO ASSINANTE NA ASSINATURA
      data.append("incluirEmail", incluirEmail);
    }

      // SE SERÁ INCLUSO OU NÃO A IMAGEM
      data.append("incluirIMG", incluirIMG);
      // SE SERÁ INCLUSO OU NÃO O TEXTO
      data.append("incluirTXT", incluirTXT);

    try {
      // FAZ A REQUISIÇÃO DE ASSINATURA PARA O BACKEND
      const response = await axios.post("/assinador/assinarKMS", data);
      // FAZ DOWNLOAD DO ARQUIVO COM A HREF QUE VEM NA RESPOSTA DA REQUISIÇÃO
      console.log(response.data);
      window.location.href = response.data;
    } catch (err) {
      // CASO OCORRA ALGUM ERRO NA REQUISIÇÃO, MOSTRA UMA MENSAGEM PARA O USUÁRIO
      console.log(err);
      try {
        window.alert("Erro ao assinar o documento: " + JSON.parse(err.response.data).message);
      } catch (errAlert) {
        window.alert("Erro ao assinar documento: " + err.response.data.message);
      }
    }
    setLoading(false);
  }

  // FORMATAÇÃO DE MENSAGEM DE ERRO CASO O CPF DO USUÁRIO SEJA INVALIDO
  const ErrorValidationLabel = ({ txtLbl }) => (
    <label htmlFor="" style={{ color: "red" }}>
      {txtLbl}
    </label>
  );
  // HTML(JSX) DA PÁGINA  
  return (
    <>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
      <h2>Assinador BRy PDF (KMS)</h2>
      {/* FORMULÁRIO PARA PEGAR OS DADOS DO USUÁRIO */}
      <form onSubmit={handleSubmit}>
        <label htmlFor="tipo_credencial">Tipo da Credencial *</label>
        <select
          name="tipo_credencial"
          value={tipo_credencial}
          required
          onChange={event => setTipoCredencial(event.target.value)}
        >
          <option value="BRYKMS">BRYKMS</option>
          <option value="DINAMO">DINAMO</option>
          <option value="GOVBR">GOVBR</option>
        </select>

        <label htmlFor="valor_credencial">Valor da Credencial - Caso seja PIN do KMS ou AToken DINAMO, deve estar em Base64 *</label>
          <input
            id="valor_credencial"
            required
            placeholder="BRYKMS - PIN/TOKEN/USER | DINAMO - ATOKEN, PIN, OTP | GOVBR - TOKEN"
            value={valor_credencial}
            type={tipo_credencial === "BRYKMS" ? ("password") : ("text")}
            onChange={event => setValorCredencial(event.target.value)}
          />

        <label htmlFor="docLabel">Documento a ser assinado *</label>
        <label htmlFor="documento" className="fileUp">
          {documento ? (
            <React.Fragment>
              {documento.name}
            </React.Fragment>
          ) : (
              <React.Fragment>
                <i className="fa fa-upload"></i>
                Selecione o arquivo
              </React.Fragment>
            )}
          <input
            id="documento"
            type="file"
            accept=".pdf"
            required
            onChange={event => setDocumento(event.target.files[0])}
          />
        </label>
        
        {tipo_credencial !== "GOVBR" ? (<></>) : 
        (<>
          <label htmlFor="cpf">CPF</label>
          <input
            id="cpf"
            type="number"
            placeholder="CPF do Assinante"
            value={cpf}
            onChange={event => setCpf(event.target.value)}
          />
          {cpfInvalido ? <ErrorValidationLabel txtLbl="CPF Invalido" /> : ""}
        </>)}

        {tipo_credencial !== "DINAMO" ? (<></>) : 
        (<>
          <label htmlFor="uuid_pkey">ID da Private Key no DINAMO *</label>
          <input
            id="uuid_pkey"
            type="text"
            placeholder="ID da Private Key no DINAMO"
            value={uuid_pkey}
            onChange={event => setUUidPkey(event.target.value)}
          />
          <label htmlFor="uuid_cert">UUID do Certificado Digital no DINAMO *</label>
          <input
            id="uuid_cert"
            type="text"
            placeholder="UUID do Certificado Digital no DINAMO"
            value={uuid_cert}
            onChange={event => setUuidCert(event.target.value)}
          />
          <label htmlFor="user">Usuário da conta no DINAMO, caso o valor repassada em Valor Credencial seja um PIN</label>
          <input
            id="user"
            type="text"
            placeholder="Usuário da conta no DINAMO"
            value={user}
            onChange={event => setUser(event.target.value)}
          />
          <label htmlFor="tipo_credencial_sistema">Tipo da Credencial - DINAMO *</label>
            <select
              name="tipo_credencial_sistema"
              value={tipo_credencial_sistema}
              required
              onChange={event => setTipoCredencialSistema(event.target.value)}
            >
              <option value="PIN">PIN</option>
              <option value="TOKEN">TOKEN</option>
              <option value="OTP">OTP</option>
        </select>
        </>)}

        {tipo_credencial !== "BRYKMS" ? (<></>) : 
        (<>
          <label htmlFor="uuid_cert">UUID do Certificado no BRy KMS (forma recomendada de selecionar o certificado)</label>
          <input
            id="uuid_cert"
            type="text"
            placeholder="UUID do Certificado Digital no BRy KMS"
            value={uuid_cert}
            onChange={event => setUuidCert(event.target.value)}
          />
          <label htmlFor="user">CPF / CNPJ do usuário da conta no BRy KMS (Busca o melhor certificado na conta automaticamente)</label>
          <input
            id="user"
            type="text"
            placeholder="Usuário da conta no BRy KMS"
            value={user}
            onChange={event => setUser(event.target.value)}
          />
          <label htmlFor="tipo_credencial_sistema">Tipo da Credencial - BRy KMS*</label>
            <select
              name="tipo_credencial_sistema"
              value={tipo_credencial_sistema}
              required
              onChange={event => setTipoCredencialSistema(event.target.value)}
            >
              <option value="PIN">PIN</option>
              <option value="TOKEN">TOKEN</option>
        </select>
        </>)}

        <label htmlFor="perfil">Perfil de Assinatura *</label>
        <select
          name="perfil"
          value={perfil}
          onChange={event => setPerfil(event.target.value)}
        >
          <option value="BASICA">Basica</option>
          <option value="CARIMBO">Com carimbo do tempo</option>
        </select>

        <label htmlFor="algoritmoHash">Algoritmo Hash *</label>
        <select
          name="algoritmoHash"
          value={algoritmoHash}
          onChange={event => setAlgoritmoHash(event.target.value)}
        >
          <option value="SHA1">SHA1</option>
          <option value="SHA256">SHA256</option>
          <option value="SHA512">SHA512</option>
        </select>

        <label htmlFor="assinaturaVisivel">Assinatura visível? *</label>
        <select
          name="assinaturaVisivel"
          value={assinaturaVisivel}
          onChange={event => setAssinaturaVisivel(event.target.value)}
        >
          <option value="false" >Não</option>
          <option value="true" >Sim</option>
        </select>

        {assinaturaVisivel === "true" ? (
              <>
               <label htmlFor="altura">Altura do campo de assinaura *</label>
                <input
                  id="altura"
                  type="number"
                  required
                  placeholder="Altura do campo de assinaura"
                  value={altura}
                  onChange={event => setAltura(event.target.value)}
                />

                <label htmlFor="largura">Largura do campo de assinaura *</label>
                <input
                  id="largura"
                  type="number"
                  required
                  placeholder="Largura do campo de assinaura"
                  value={largura}
                  onChange={event => setLargura(event.target.value)}
                />

                <label htmlFor="coordenadaX">Coordenada X *</label>
                <input
                  id="coordenadaX"
                  type="number"
                  required
                  placeholder="Coordenada X do campo de assinaura"
                  value={coordenadaX}
                  onChange={event => setCoordenadaX(event.target.value)}
                />

                <label htmlFor="coordenadaY">Coordenada Y *</label>
                <input
                  id="coordenadaY"
                  type="number"
                  required
                  placeholder="Coordenada Y do campo de assinaura"
                  value={coordenadaY}
                  onChange={event => setCoordenadaY(event.target.value)}
                />

                <label htmlFor="posicao">Posicao do campo de assinaura *</label>
                <select
                  name="posicao"
                  value={posicao}
                  onChange={event => setPosicao(event.target.value)}
                >
                  <option value="INFERIOR_ESQUERDO">Inferior Esquerdo</option>
                  <option value="INFERIOR_DIREITO">Inferior Direito</option>
                  <option value="SUPERIOR_ESQUERDO">Superior Esquerdo</option>
                  <option value="SUPERIOR_DIREITO">Superior Direito</option>
                </select>

                <label htmlFor="PAGINA">Página do campo de assinaura *</label>
                <select
                  name="pagina"
                  value={pagina}
                  onChange={event => setPagina(event.target.value)}
                >
                  <option value="PRIMEIRA">Primeira</option>
                  <option value="ULTIMA">Última</option>
                  <option value="TODAS">Todas</option>
                </select>

                <label htmlFor="incluirIMG">Incluir imagem? *</label>
            <select
              name="incluirIMG"
              value={incluirIMG}
              onChange={event => setIncluirIMG(event.target.value)}
            >
              <option value="false" >Não</option>
              <option value="true" >Sim</option>
            </select>

            {incluirIMG === "true" ? (
              <>
                <h2>Configurações de imagem</h2>

                <label htmlFor="lblImagem">Imagem *</label>

                <label htmlFor="imagem" className="fileUp">
                  {imagem ? (
                    <React.Fragment>{imagem.name}</React.Fragment>
                  ) : (
                      <React.Fragment>
                        <i className="fa fa-upload"></i>
                      Selecione a imagem de assinatura
                      </React.Fragment>
                    )}

                  <input
                    id="imagem"
                    type="file"
                    accept=".png, .jpeg, .jpg, .bmp"
                    required
                    onChange={event => setImagem(event.target.files[0])}
                  />
                </label>
               
              </>
            ) : ("")}
                <label htmlFor="incluirTXT">Incluir texto? *</label>
                <select
                  name="incluirTXT"
                  value={incluirTXT}
                  onChange={event => setIncluirTXT(event.target.value)}
                >
                  <option value="false" >Não</option>
                  <option value="true" >Sim</option>
                </select>

                {incluirTXT === "true" ? (
                  <>
                    <label htmlFor="texto">Texto de Assinatura*</label>
                    <input
                      id="texto"
                      type="text"
                      required
                      placeholder="Texto incluso na Assinatura"
                      value={texto}
                      onChange={event => setTexto(event.target.value)}
                      />

                  </>
                ) : (
                  ""
                )}

                <label htmlFor="incluirCPF">Incluir CPF na Assinatura?</label>
                <select
                  name="incluirCPF"
                  value={incluirCPF}
                  onChange={event => setIncluirCPF(event.target.value)}
                >
                  <option value="false" >Não</option>
                  <option value="true" >Sim</option>
                </select>

                <label htmlFor="incluirEmail">Incluir Email na Assinatura?</label>
                <select
                  name="incluirEmail"
                  value={incluirEmail}
                  onChange={event => setIncluirEmail(event.target.value)}
                >
                  <option value="false" >Não</option>
                  <option value="true" >Sim</option>
                </select>

                <label htmlFor="incluirCN">Incluir Nome na Assinatura?</label>
                <select
                  name="incluirCN"
                  value={incluirCN}
                  onChange={event => setIncluirCN(event.target.value)}
                >
                  <option value="false" >Não</option>
                  <option value="true" >Sim</option>
                </select>
                  </>
                ) : ("")}


        <button className="btn" type="submit">
          Assinar
        </button>

        <label>
          {loading ? "Realizando a assinatura do documento..." : ""}
        </label>
      </form>
    </>
  );
}
