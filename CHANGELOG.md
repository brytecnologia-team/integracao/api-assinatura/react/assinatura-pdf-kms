# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.0.0] - 2020-08-05

### Added

- A SIGNATURE FRONTEND example with BRy KMS.
- README
- CHANGELOG
- LICENSE

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/react/assinatura-pdf-kms/-/tags/1.0.0

